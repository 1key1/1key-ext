import cn from '_@/utils/cn';
import { ComponentPropsWithoutRef, forwardRef } from 'react';

const baseClasses = ['outline-none btn-md'];
const colorClasses = {
  primary: {
    filled: ['bg-primary-500 text-white hover:bg-primary-600 focus:bg-primary-600'],
    outline: [' border border-primary-500 text-primary-500 hover:bg-primary-50'],
    ghost: ['text-primary-500 hover:bg-primary-50'],
  },
  secondary: {
    filled: [''],
    outline: [''],
    ghost: [''],
  },
  error: {
    filled: [''],
    outline: [''],
    ghost: [''],
  },
};

type Color = keyof typeof colorClasses;
type Variant = keyof (typeof colorClasses)[Color];
type ButtonProps = {
  variant?: Variant;
  color?: Color;
} & ComponentPropsWithoutRef<'button'>;

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  ({ variant = 'filled', color = 'primary', className, children, ...props }, ref) => {
    return (
      <button
        ref={ref}
        className={cn([baseClasses, colorClasses[color][variant], className])}
        {...props}
      >
        {children}
      </button>
    );
  },
);
Button.displayName = 'Button';
export default Button;
