'use client';
import { ComponentPropsWithoutRef, forwardRef, useRef } from 'react';

import Show from '../Show';
import cn from '_@/utils/cn';

const baseClasses = [
  'border border-gray-300 outline-none text-text-10 placeholder:text-text-50 w-full',
  'hover:ring-1 hover:ring-primary-500 hover:border-transparent',
  'focus:ring-1 focus:ring-primary-500 focus:border-transparent',
  'data-[valid=false]:ring-1 data-[valid=false]:ring-red-500 data-[valid=false]:border-transparent',
];

const inputSizes = {
  sm: 'input-sm',
  md: 'input-md',
  lg: 'input-lg',
};

export type BaseInputProps = {
  isValid?: boolean;
  inputSize?: 'sm' | 'md' | 'lg';
  leadingComponent?: React.ReactNode;
  trailingComponent?: React.ReactNode;
} & ComponentPropsWithoutRef<'input'>;
const BaseInput = forwardRef<HTMLInputElement, BaseInputProps>(
  (
    { leadingComponent, trailingComponent, className, isValid = true, inputSize = 'md', ...props },
    ref,
  ) => {
    const trailingRef = useRef<HTMLDivElement>(null);
    return (
      <div className="relative">
        <Show when={!!leadingComponent}>
          <div ref={trailingRef} className="absolute left-2 top-1/2 -translate-y-1/2">
            {leadingComponent}
          </div>
        </Show>
        <input
          {...props}
          ref={ref}
          data-valid={isValid}
          className={cn([
            baseClasses,
            inputSizes[inputSize],
            trailingComponent ? 'pr-10' : 'pr-2',
            leadingComponent ? 'pl-10' : 'pl-2',
            className,
          ])}
        />
        <Show when={!!trailingComponent}>
          <div ref={trailingRef} className="absolute right-2 top-1/2 -translate-y-1/2">
            {trailingComponent}
          </div>
        </Show>
      </div>
    );
  },
);

BaseInput.displayName = 'BaseInput';

export default BaseInput;
