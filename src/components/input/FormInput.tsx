import { useFormContext } from 'react-hook-form';

import BaseInput, { BaseInputProps } from './BaseInput';
import { getErrorMessage } from '_@/utils/func';

export default function FormInput({ name = '', ...props }: BaseInputProps) {
  const {
    register,
    formState: { errors },
  } = useFormContext();
  const isValid = !getErrorMessage(name, errors);

  return <BaseInput {...props} {...register(name)} isValid={isValid} />;
}
