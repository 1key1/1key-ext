import clsx from 'clsx';
import { cloneElement, useId } from 'react';
import { useFormContext } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';

import Show from './Show';

type TProps = {
  label?: string;
  name: string;
  children: JSX.Element;
  className?: string;
  required?: boolean;
};

export default function FormItem({
  label,
  name,
  children,
  className = '',
  required = false,
}: TProps) {
  const id = useId();
  const {
    formState: { errors },
  } = useFormContext();

  return (
    <div className={clsx([className])}>
      <Show when={label}>
        <label htmlFor={id} className="mb-2 block text-14 text-text-20">
          {label}
          {required ? <span className="ms-1 text-red-500">&#42;</span> : null}
        </label>
      </Show>
      {cloneElement(children, { id, name })}
      <ErrorMessage
        name={name}
        errors={errors}
        render={({ message }) => (
          <p id={`err-${id}`} className="mt-2 block text-xs text-red-500">
            {message}
          </p>
        )}
      />
    </div>
  );
}
