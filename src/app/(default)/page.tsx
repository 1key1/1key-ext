import Button from '_@/components/button/Button';
import BaseInput from '_@/components/input/BaseInput';

export default function Home() {
  return (
    <main>
      <BaseInput placeholder="enter" inputSize="md" />
      <Button className="mt-2">Click me</Button>
    </main>
  );
}
