import BaseInput from '_@/components/input/BaseInput';

export default function Header() {
  return (
    <header className="sticky top-0 left-0 h-14 px-3">
      <div className="flex items-center h-full">
        <div>Logo</div>
        <div className='ml-4'>
          <BaseInput placeholder="Search your password..." inputSize="md" />
        </div>
      </div>
    </header>
  );
}
