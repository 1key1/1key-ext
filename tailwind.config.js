import inputPlugin from './tailwind-plugins/input.js';
import buttonPlugin from './tailwind-plugins/button.js';
import spacingPlugin from './tailwind-plugins/spacing.js';
import componentPlugin from './tailwind-plugins/component.js';
import variantPlugin from './tailwind-plugins/variants.js';

module.exports = {
  content: ['./src/**/*.{js,ts,jsx,tsx,mdx}'],

  theme: {
    fontFamily: {
      inter: ['inter', 'sans-serif'],
    },
    extend: {
      screens: {
        's-1360': '1360px',
        's-1440': '1440px',
      },
      colors: {
        primary: {
          50: '#eef8ff',
          100: '#ddf0ff',
          200: '#b3e2ff',
          300: '#70cdff',
          400: '#24b5ff ',
          500: '#009aff', // bg
          600: '#007adb',
          700: '#0061b1',
          800: '#005292', // text
          900: '#02457a',
          950: '#012a50',
        },
        text: {
          DEFAULT: '#0A0A0E',
          10: '#2F3339',
          20: '#46494F',
          30: '#5D6165',
          50: '#8B8E92',
          60: '#A2A4A7',
          70: '#B9BABD',
          80: '#D0D2D3',
          100: '#FFFFFF',
          primary: '#212B36',
        },
      },
      fontSize: {
        14: ['0.875rem', '1.5rem'],
        18: ['1.125rem', '1.5rem'],
        20: ['1.25rem', '1.5rem'],
        '24/32': ['1.5rem', '2rem'],
        '24/36': ['1.5rem', '2.25rem'],
        32: ['2rem', '3rem'],
        40: ['2.5rem', '3.75rem'],
        '48/56': ['3rem', '3.5rem'],
        '48/60': ['3rem', '3.75rem'],
        56: ['3.5rem', '4.375rem'],
        64: ['4rem', '5rem'],
        80: ['5rem', '6.25rem'],
      },
      zIndex: {
        sticky: 10, // for fix / sticky
        dropdown: 20,
        overlay: 100, // for modal
        toast: 1000,
        tooltip: 1000,
      },
      animation: {
        'fade-up': 'fadeUp 500ms both',
        'fade-down': 'fadeDown 500ms both',
        'fade-right': 'fadeRight 500ms both',
        'fade-left': 'fadeLeft 500ms both',
        fade: 'fade 500ms both',
        'toast-in-left': 'toastInLeft 250ms both',
        'toast-in-right': 'toastInRight 250ms both',
      },
      keyframes: {
        fade: {
          '0%': { opacity: 0 },
          '100%': { opacity: 1 },
        },
        fadeUp: {
          '0%': { opacity: 0, transform: 'translateY(5px)' },
          '100%': { opacity: 1, transform: 'translateY(0)' },
        },
        fadeDown: {
          '0%': { opacity: 0, transform: 'translateY(-5px)' },
          '100%': { opacity: 1, transform: 'translateY(0)' },
        },
        fadeRight: {
          '0%': { opacity: 0, transform: 'translateX(-5px)' },
          '100%': { opacity: 1, transform: 'translateX(0)' },
        },
        fadeLeft: {
          '0%': { opacity: 0, transform: 'translateX(5px)' },
          '100%': { opacity: 1, transform: 'translateX(0)' },
        },
        toastInLeft: {
          '0%': { transform: 'translateX(-100%)' },
          '100%': { transform: 'translateX(0)' },
        },
        toastInRight: {
          '0%': { transform: 'translateX(100%)' },
          '100%': { transform: 'translateX(0)' },
        },
      },
      boxShadow: {
        main: 'rgba(145, 158, 171, 0.24) 0px 0px 2px 0px, rgba(145, 158, 171, 0.24) -20px 20px 40px -4px',
      },
    },
  },
  plugins: [
    inputPlugin,
    buttonPlugin,
    componentPlugin,
    variantPlugin,
    spacingPlugin({ spacing: 100 }),
  ],
};
