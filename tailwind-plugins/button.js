const plugin = require('tailwindcss/plugin');

module.exports = plugin(({ addComponents, theme }) => {
  addComponents({
    '.btn-sm': {
      height: '2rem',
      paddingLeft: '1.5rem',
      paddingRight: '1.5rem',
      fontSize: '.75rem',
      lineHeight: '1.125rem',
      borderRadius: '6px',
    },

    '.btn-md': {
      height: '2.5rem',
      paddingLeft: '1.5rem',
      paddingRight: '1.5rem',
      fontSize: '.875rem',
      lineHeight: '1.25rem',
      borderRadius: '6px',
    },
    '.btn-lg': {
      height: '3.125rem',
      paddingLeft: '1.5rem',
      paddingRight: '1.5rem',
      fontSize: '1rem',
      lineHeight: '1.5rem',
      borderRadius: '6px',
    },
  });
});
