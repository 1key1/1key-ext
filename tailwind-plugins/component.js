const plugin = require('tailwindcss/plugin');

module.exports = plugin(({ addComponents, theme }) => {
  addComponents({
    '.icon-sm': {
      height: '1rem',
      width: '1rem',
    },

    '.icon-md': {
      height: '1.25rem',
      width: '1.25rem',
    },
  });
  addComponents({
    '.text-gradient': {
      background: 'linear-gradient(rgb(66, 230, 149), rgb(59, 178, 184))',
      WebkitBackgroundClip: 'text',
      WebkitTextFillColor: 'transparent',
    },
  });
});
