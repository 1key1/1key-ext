const plugin = require('tailwindcss/plugin');

module.exports = plugin(({ addComponents, theme }) => {
  addComponents({
    '.input-sm': {
      height: '2rem',
      fontSize: '.75rem',
      lineHeight: '1.125rem',
      borderRadius: '6px',
    },

    '.input-md': {
      height: '2.5rem',
      fontSize: '.875rem',
      lineHeight: '1.25rem',
      borderRadius: '6px',
    },
    '.input-lg': {
      height: '3.125rem',
      fontSize: '1rem',
      lineHeight: '1.5rem',
      borderRadius: '6px',
    },
  });
});
